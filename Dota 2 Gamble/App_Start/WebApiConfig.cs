﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Dota_2_Gamble
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "RouteApiWithAction",
                routeTemplate: "api/{controller}/{action}/{nbElements}",
                defaults: new { nbElements = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            config.Routes.MapHttpRoute(
                name: "RouteApiWithActionAndMarketHashName",
                routeTemplate: "api/{controller}/{action}/{Market_Hash_Name}",
                defaults: new { }
            );

            config.Routes.MapHttpRoute(
                name: "RouteApiWithInstanceIDANDclassID",
                routeTemplate: "api/{controller}/{action}/{classid:long}/{instanceid:long}",
                defaults: new {  }
            );
        }
    }
}
