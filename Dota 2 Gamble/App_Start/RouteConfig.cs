﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Dota_2_Gamble
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.AppendTrailingSlash = true;
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "RouteApiWithActionAndMarketHashName2",
                url: "api/{controller}/{action}/{Market_Hash_Name}",
                defaults: new {}
            );

            routes.MapRoute(
                name: "RouteApiWithInstanceIDANDclassID2",
                url: "api/{controller}/{action}/{classid:long}/{instanceid:long}",
                defaults: new { }
            );
        }
    }
}
