﻿namespace Dota_2_Gamble.Models
{
    public class Rarity
    { 
       

    private int rarityLevel;
        private int v;

        public Rarity(int v)
        {
            this.v = v;
        }

    public bool equals(object o)
        {
            if (this == o) return true;
            if (!(o is Rarity)) return false;

            Rarity rarity = (Rarity)o;

            return rarityLevel == rarity.rarityLevel;

        }
        
    public int hashCode()
        {
            return rarityLevel;
        }
    }
}