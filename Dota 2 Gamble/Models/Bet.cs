﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dota_2_Gamble.Models
{
    public class Bet
    {
        private Player _player;
        private List<Dota2Item> itemsBet;

        public Player Player
        {
            get
            {
                return _player;
            }

            set
            {
                _player = value;
            }
        }

        public List<Dota2Item> ItemsBet
        {
            get
            {
                return itemsBet;
            }

            set
            {
                itemsBet = value;
            }
        }

        public double computeBetPot()
        {
            double pot = 0;
            foreach(Dota2Item item in itemsBet)
            {
                pot += item.Price;
            }
            return pot;
        }
    }
}