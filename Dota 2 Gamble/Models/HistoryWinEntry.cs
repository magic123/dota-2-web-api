﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dota_2_Gamble.Models
{
    public class HistoryWinEntry
    {
        private Player winner;
        private List<Dota2Item> itemsWon;
        private double pot;
        private DateTime date;

        public Player Winner
        {
            get
            {
                return winner;
            }

            set
            {
                winner = value;
            }
        }

        public List<Dota2Item> ItemsWon
        {
            get
            {
                return itemsWon;
            }

            set
            {
                itemsWon = value;
            }
        }

        public double Pot
        {
            get
            {
                return pot;
            }

            set
            {
                pot = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        private HistoryWinEntry(Player winner1, List<Dota2Item> itemsWon1, double v, DateTime date)
        {
            this.Winner = winner1;
            this.ItemsWon = itemsWon1;
            this.Pot = v;
            this.Date = date;
        }

        public class Builder
        {
            private List<Dota2Item> itemsWon;
            private double pot;
            private Player winner;
            private DateTime Date;

            public Builder ItemWon(List<Dota2Item> items)
            {
                this.itemsWon = items;
                return this;
            }

            public Builder Pot(double pot)
            {
                this.pot = pot;
                return this;
            }

            public Builder Winner(Player winner)
            {
                this.winner = winner;
                return this;
            }

            public Builder SetDate(long unixTime)
            {
                // Unix timestamp is seconds past epoch
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(unixTime).ToLocalTime();
                return this;
            }

            public HistoryWinEntry Build()
            {
                return new HistoryWinEntry(this.winner, this.itemsWon, this.pot, this.Date);
            }
        }


    }
}