﻿namespace Dota_2_Gamble.Models
{
    public class Player
    {
        /*
        ID int identity(1,1),
SteamID bigint,
FirstName nvarchar(55),
LastName nvarchar(55),
URLTrade nvarchar(55),
PlayerURLLogo nvarchar(120),
constraint PK_Player_ID primary key (ID),
constraint UC_SteamID UNIQUE (SteamID)
        */
        private int _ID;
        private long _steamID;
        private string _lastName;
        private string _firstName;
        private string _URLTrade;
        private string _PlayerURLLogo;

        public Player()
        {

        }

        public int ID
        {
            get
            {
                return _ID;
            }

            set
            {
                _ID = value;
            }
        }

        public long SteamID
        {
            get
            {
                return _steamID;
            }

            set
            {
                _steamID = value;
            }
        }

        public string LastName
        {
            get
            {
                return _lastName;
            }

            set
            {
                _lastName = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _firstName;
            }

            set
            {
                _firstName = value;
            }
        }

        public string URLTrade
        {
            get
            {
                return _URLTrade;
            }

            set
            {
                _URLTrade = value;
            }
        }

        public string PlayerURLLogo
        {
            get
            {
                return _PlayerURLLogo;
            }

            set
            {
                _PlayerURLLogo = value;
            }
        }
    }
}