﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dota_2_Gamble.Models
{
    public class Constants
    {
        public const String PROFILE_BASE_URL = "http://steamcommunity.com/profiles/";
        public const String PROFILE_SUFIX_URL = "inventory/json/570/2";

        public const string START_NEW_BET_TOPIC = "NewGameBet";
    }
}
