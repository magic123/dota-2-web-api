﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dota_2_Gamble.Models
{
    public class Dota2Item
    {
        public static Rarity COMMON = new Rarity(0);
        public static Rarity UNCOMMON = new Rarity(0);
        public static Rarity RARE = new Rarity(0);
        public static Rarity MYTHICAL = new Rarity(0);
        public static Rarity LEGENDARY = new Rarity(0);
        public static Rarity ANCIENT = new Rarity(0);
        public static Rarity IMMORTAL = new Rarity(0);
        public static Rarity ARCANA = new Rarity(0);
        public static Rarity SEASONAL = new Rarity(0);

        private long _id;
        private long _instanceID;
        private long _classID;
        private String _iconURL;
        private String _marketName;
        private String _MarketHashName;
        private Rarity _rarity;
        private double _price;

        public string MarketHashName
        {
            get
            {
                return _MarketHashName;
            }

            set
            {
                _MarketHashName = value;
            }
        }

        public long Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public long InstanceID
        {
            get
            {
                return _instanceID;
            }

            set
            {
                _instanceID = value;
            }
        }

        public long ClassID
        {
            get
            {
                return _classID;
            }

            set
            {
                _classID = value;
            }
        }

        public string IconURL
        {
            get
            {
                return _iconURL;
            }

            set
            {
                _iconURL = value;
            }
        }

        public string MarketName
        {
            get
            {
                return _marketName;
            }

            set
            {
                _marketName = value;
            }
        }


        public Rarity Rarity
        {
            get
            {
                return _rarity;
            }

            set
            {
                _rarity = value;
            }
        }

        public double Price
        {
            get
            {
                return _price;
            }

            set
            {
                _price = value;
            }
        }

        public Dota2Item()
        {
        }

       

       
    }
}