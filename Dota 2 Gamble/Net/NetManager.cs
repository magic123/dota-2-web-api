﻿using Dota_2_Gamble.Models;
using Dota_2_Gamble.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using static Dota_2_Gamble.Models.Constants;

namespace Dota_2_Gamble.Net
{
    public class NetManager
    {
        private const string MessageType = "application/json";
        private const string BASE_ADDRESS = "http://backpack.tf/api/";
        private const string SUFFIX_ADDRESS = "IGetMarketPrices/v1/?key=56cac6f3b98d885f53dcea6c&appid=570&format=json";
        private const String GCM_BASE_ADDRESS = "https://gcm-http.googleapis.com/gcm/";

        private static NetManager instance;

        private NetManager() { }

        public static NetManager GetNetManagerInstance()
        {
            if (instance == null)
                instance = new NetManager();
            return instance;
        }

        private String ApiKey = "AIzaSyBhxibDSbdG7ZEjx8HWXgZ-LqJLnF8RERQ";

        public static HttpClient GetHttpClient(String BaseAddress)
        {
            HttpClient c = new HttpClient();
            c.BaseAddress = new Uri(BaseAddress);
            c.Timeout = TimeSpan.FromSeconds(5);
            c.DefaultRequestHeaders.Accept.Clear();
            c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MessageType));
            return c;
        }

        public static String BuildPlayerInventoryURL(long SteamID)
        {
            return String.Format("{1}/{2}", PROFILE_BASE_URL, SteamID, PROFILE_SUFIX_URL);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "\\W", "_", RegexOptions.IgnoreCase);
        }

        public static void updateItemsFromWebApi()
        {
            HttpResponseMessage responseMessage = GetHttpClient(BASE_ADDRESS).GetAsync(SUFFIX_ADDRESS).Result;
            string itemsAsJsonString = responseMessage.Content.ReadAsStringAsync().Result;

            JObject itemsAsJsonObject = JObject.Parse(itemsAsJsonString);

            JToken j = itemsAsJsonObject.Property("response").Value;
            JObject response = j.Value<JObject>();

            JToken responseCode = response.GetValue("success");
            if (responseCode.ToString().Equals("1"))
            {
                JObject itemsAsJSON = response.GetValue("items").Value<JObject>();
                Dota2Item currentItem;
                foreach (KeyValuePair<string, JToken> property in itemsAsJSON)
                {
                    currentItem = new Dota2Item();
                    currentItem.Price = ((double)property.Value.Value<JObject>()["value"].Value<double>() / 100);
                    currentItem.MarketHashName = RemoveSpecialCharacters(property.Key);
                    DBService.GetDBInstance().updatePrice(currentItem);
                }

            }
            else
            {
                Console.WriteLine("Response code is ERROR : " + response.GetValue("message"));
            }
        }

        /// <summary>
        /// Sends a notification to player that a new game has started
        /// </summary>
        public void NotifyPlayersPotChanged()
        {
            HttpClient client = GetHttpClient(GCM_BASE_ADDRESS);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", String.Format("key={0}", ApiKey));

            JObject JSONObject = createDefaultJSONObjectForGCM("bet");
            JObject JSONData = JSONObject["data"].Value<JObject>();
            JSONData.Add("message", "REFRESH");
            String JSONstr = JSONObject.ToString();
            HttpResponseMessage messageResponse = client.PostAsync("send", new StringContent(JSONObject.ToString(), Encoding.UTF8, "application/json")).Result;
            String responseMessage = messageResponse.Content.ReadAsStringAsync().Result;
        }

        /// <summary>
        /// Send a message to user telling them a new game is about to start !
        /// </summary>
        public void SendAStartGame(DateTimeOffset OffSet)
        {
            HttpClient client = GetHttpClient(GCM_BASE_ADDRESS);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", String.Format("key={0}", ApiKey));
            
            JObject JSONObject = createDefaultJSONObjectForGCM("bet");

            JObject JSONData = JSONObject["data"].Value<JObject>();
            JSONData.Add("message", new JValue("START"));
            JSONData.Add("endtime", new JValue(OffSet.Ticks));

            String JSONstr = JSONObject.ToString();            
            HttpResponseMessage messageResponse = client.PostAsync("send", new StringContent(JSONObject.ToString(), Encoding.UTF8, "application/json")).Result;
            String responseMessage = messageResponse.Content.ReadAsStringAsync().Result;
        }

        public JObject createDefaultJSONObjectForGCM(String SuffixTopic)
        {
            JObject JSONObject = new JObject();
            JSONObject.Add("to", new JValue(String.Format("/topics/{0}",SuffixTopic)));
            JSONObject.Add("data", new JObject());

            return JSONObject;
        }


    }
}