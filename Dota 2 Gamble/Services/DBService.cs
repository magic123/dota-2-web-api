﻿using Dota_2_Gamble.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dota_2_Gamble.Net.NetManager;

namespace Dota_2_Gamble.Services
{
    class DBService
    {
        private static DBService service;
        private static String STRINGCONNECTION = "toto";
        private const long NICK_STEAM_ID = 76561197972267880L;

        private DBService()
        {

        }

        public static DBService GetDBInstance()
        {
            if (service == null)
                service = new DBService();
            return service;
        }

        public static DBService getDBInstance()
        {
            if (service == null)
                service = new DBService();
            return service;
        }


        public List<Dota2Item> getAllItems()
        {
            List<Dota2Item> items = new List<Dota2Item>();
            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * from Dota2Item";
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                Dota2Item item = null;
                foreach (DataRow dr in dt.Rows)
                {
                    item = new Dota2Item();
                    item.IconURL = (string)dr["IconURL"];
                    item.Id = (long)dr["ID"];
                    item.MarketName = (string)dr["Market_Hash_Name"];
                    item.Price = (double)dr["Price"];
                    item.ClassID = (long)dr["ClassID"];
                    item.InstanceID = (long)dr["InstanceID"];
                    items.Add(item);
                }
            }
            return items;
        }

        public double GetPrice(string market_Hash_Name)
        {
            double price = 0.0;
            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * from Dota2Item where Market_Hash_Name = @Market_Hash_Name";
                command.Parameters.Add(new SqlParameter("Market_Hash_Name", market_Hash_Name));
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                if (dt.Rows != null && dt.Rows[0] != null)
                {
                    DataRow firstRow = dt.Rows[0];
                    price = decimal.ToDouble((decimal)firstRow["Price"]);
                }
            }
            return price;
        }

        public double getPrice(double classID, double instanceID)
        {
            double price = -1.0;
            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            string Query = "Select (Price) from ItemPrice where classID = @classID AND instanceID = @instanceID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = Query;

                SqlParameter parameter1 = new SqlParameter("classID", classID);
                SqlParameter parameter2 = new SqlParameter("instanceID", instanceID);
                command.Parameters.Add(parameter1);
                command.Parameters.Add(parameter2);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                if (dt.Rows != null && dt.Rows[0] != null)
                    price = (double)dt.Rows[0]["Price"];
            }
            return price;
        }

        public List<HistoryWinEntry> getLatestHistoryEntries()
        {
            List<HistoryWinEntry> historyEntries = new List<HistoryWinEntry>(5);

            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            string Query1 = "SELECT TOP 5 * FROM WinsHistory wh ORDER BY wh.WinDate DESC";
            string Query2 = "SELECT * FROM WinItemsHistory iwh WHERE iwh.WinHistoryID = @ID";


            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = Query1;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    int id = (int)row["ID"];
                    long SteamID = (long)row["Winner"];
                    double pot = decimal.ToDouble((decimal)row["PotValue"]);
                    HistoryWinEntry.Builder entry = new HistoryWinEntry.Builder();
                    List<Dota2Item> items = new List<Dota2Item>();
                    using (SqlConnection connection2 = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command2 = connection.CreateCommand();
                        command2.CommandText = Query2;
                        command2.Parameters.Add(new SqlParameter("ID", id));
                        SqlDataAdapter adapter2 = new SqlDataAdapter(command2);
                        DataTable dt2 = new DataTable();
                        adapter2.Fill(dt2);

                        foreach (DataRow row2 in dt2.Rows)
                        {
                            items.Add(getDota2Item(row2["ItemWon"] as string));
                        }

                    }
                    historyEntries.Add(entry.ItemWon(items).Pot(pot).Winner(getPlayer(SteamID)).Build());
                }
            }
            return historyEntries;
        }

        public Player getPlayer(long steamID)
        {
            Player player = new Player();
            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * from Player where SteamID = @steamID";
                command.Parameters.Add(new SqlParameter("steamID", steamID));

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    player.FirstName = (string)dr["FirstName"];
                    player.LastName = (string)dr["LastName"];
                    player.ID = (int)dr["ID"];
                    player.SteamID = steamID;
                }
            }
            return player;
        }

        public Dota2Item getDota2Item(string market_hash_name)
        {
            Dota2Item item = new Dota2Item();
            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * from Dota2Item where @Market_Hash_Name = Market_Hash_Name";
                command.Parameters.Add(new SqlParameter("Market_Hash_Name", market_hash_name));

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    item.Id = (int)dr["ID"];
                    item.MarketHashName = (string)dr["Market_Hash_Name"];
                    item.MarketName = dr["Market_Name"] as string;
                    item.IconURL = dr["Icon_URL"] as string;
                    item.Price = (decimal.ToDouble((decimal)dr["Price"]));
                }
            }
            return item;
        }

        public void insertOrUpdate(Dota2Item item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand("usp_Update_Inventory", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("Market_Hash_Name", item.MarketHashName));
                    command.Parameters.Add(new SqlParameter("Market_Name", item.MarketName));
                    command.Parameters.Add(new SqlParameter("Icon_URL", item.IconURL));


                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (SqlException e)
                {
                    if (e.Number != 2627)
                    {
                        throw e;
                    }
                }

            }
        }

        public void updatePrice(Dota2Item item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString))
            {
                try
                {
                    SqlCommand command = connection.CreateCommand();

                    String query = "UPDATE Dota2Item SET Price = @Price where Market_Hash_Name = @Market_Hash_Name";
                    command.CommandText = query;

                    command.Parameters.Add(new SqlParameter("PRICE", item.Price));
                    command.Parameters.Add(new SqlParameter("Market_Hash_Name", item.MarketHashName));

                    connection.Open();
                    command.ExecuteScalar();
                    connection.Close();
                }
                catch (SqlException e)
                {
                    Console.Error.WriteLine(e.ToString());
                }
            }
        }

        public List<Dota2Item> getItemsWithIconURL()
        {
            List<Dota2Item> items = new List<Dota2Item>();
            string ConnectionString = ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * from Dota2Item where Icon_URL is not null";

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                Dota2Item item = null;
                foreach (DataRow dr in dt.Rows)
                {
                    item = new Dota2Item();
                    item.Id = (int)dr["ID"];
                    item.MarketHashName = (string)dr["Market_Hash_Name"];
                    item.MarketName = dr["Market_Name"] as string;
                    item.IconURL = dr["Icon_URL"] as string;
                    item.Price = (decimal.ToDouble((decimal)dr["Price"]));
                    items.Add(item);
                }
            }
            return items;
        }

        public void GenerateWinHistoryDB()
        {
            List<Dota2Item> items = getItemsWithIconURL();
            Random Generator = new Random();
            for (int i = 0; i < 5; i++)
            {
                int numberOfItems = Generator.Next(15, 25);
                List<Dota2Item> itemsWon = new List<Dota2Item>();
                for (int j = 0; j < numberOfItems; j++)
                {
                    int index = Generator.Next(0, items.Count - 1);
                    itemsWon.Add(items[index]);
                }
                InsertANewWinHistory(NICK_STEAM_ID, itemsWon);
            }
        }

        private long InsertANewWinHistoryHelper(long steamID, HistoryWinEntry entry)
        {
            int id = -1;
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString))
            {
                try
                {
                    SqlCommand command = connection.CreateCommand();
                    command.Parameters.Add(new SqlParameter("WinnerID", entry.Winner.SteamID));
                    command.Parameters.Add(new SqlParameter("PotValue", entry.Pot));
                    command.Parameters.Add(new SqlParameter("WinDate", entry.Date.Ticks));

                    command.CommandText = " INSERT INTO [WinsHistory] ([PotValue], [Winner], [WinDate]) OUTPUT INSERTED.ID VALUES (@PotValue, @WinnerID, @WinDate)";

                    connection.Open();
                    id =  (int)command.ExecuteScalar();
                    connection.Close();
                }
                catch (SqlException e)
                {
                    if (e.Number != 2627)
                    {
                        throw e;
                    }
                }
            }
            return id;
        }

        private void InsertANewWinHistory(long steamID, List<Dota2Item> items)
        {
            HistoryWinEntry entry = new HistoryWinEntry.Builder().ItemWon(items).Winner(new Player() { SteamID = NICK_STEAM_ID }).Pot(ComputePotValue(items)).SetDate(1393857385L + new Random().Next(-1000, 1000)).Build();


            long newID = InsertANewWinHistoryHelper(steamID, entry);

            foreach (Dota2Item itemWon in items) {
                InsertItemIntoWinHistory(itemWon, NICK_STEAM_ID, newID);
            }
        }

        private void InsertItemIntoWinHistory(Dota2Item itemWon, long nICK_STEAM_ID, long WINID)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[STRINGCONNECTION].ConnectionString))
            {
                try
                {
                    SqlCommand command = connection.CreateCommand();
                    command.Parameters.Add(new SqlParameter("ItemWon", itemWon.MarketHashName));
                    command.Parameters.Add(new SqlParameter("WinHistoryID",  WINID));

                    command.CommandText = "INSERT INTO WinItemsHistory (ItemWon, WinHistoryID) values (@ItemWon,@WinHistoryID)";

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (SqlException e)
                {
                    if (e.Number != 2627)
                    {
                        throw e;
                    }
                }

            }
        }

        private double ComputePotValue(List<Dota2Item> items)
        {
            double pot = 0.0;
            foreach (Dota2Item item in items)
            {
                pot += item.Price;
            }
            return pot;
        }
    }
}
