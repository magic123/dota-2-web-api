﻿using Dota_2_Gamble.Models;
using Dota_2_Gamble.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dota_2_Gamble.Controllers
{
    public class WinsController : ApiController
    {   
             

        [HttpGet]
        public IEnumerable<HistoryWinEntry> lastWins()
        {
            return DBService.GetDBInstance().getLatestHistoryEntries();
        }

    }
}
