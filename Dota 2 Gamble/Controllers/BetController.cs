﻿using Dota_2_Gamble.Models;
using Dota_2_Gamble.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static Dota_2_Gamble.Net.NetManager;

namespace Dota_2_Gamble.Controllers
{
    public class BetController : ApiController
    {
        private List<Bet> Bets;
        private NetManager NetManagerInstance = GetNetManagerInstance();
        private DateTimeOffset OffSetTime;

        /// <summary>
        /// This is called when a user put some items on a bet
        /// </summary>
        /// <param name="items"></param>
        public void putItemsOnBet(long SteamID, List<Dota2Item> items)
        {
            if (Bets == null || Bets.Count == 0)
            {
                // New Game, this is the first bet
                Bets = new List<Bet>();
                InsertNewBet(SteamID, items);
            } else if (!isFirstBet(SteamID))
            {
                incrementExistingBet(SteamID, items);
            }
            else
            {
                if (Bets.Count == 1)
                {
                    // We can start a new game
                    InsertNewBet(SteamID, items);
                    // Game starts !
                    NetManagerInstance.SendAStartGame(ComputeBetEndTime());

                }
                else if (Bets.Count >= 2)
                {
                    InsertNewBet(SteamID, items);
                }
                else
                {
                    //The same player is betting again
                    incrementExistingBet(SteamID, items);
                }   
            }
        }


        private void incrementExistingBet(long steamID, List<Dota2Item> items)
        {
            foreach (Bet _Bet in Bets)
            {
                if (_Bet.Player.SteamID == steamID)
                {
                    _Bet.ItemsBet.InsertRange(_Bet.ItemsBet.Count - 1, items);
                }
            }
            NetManagerInstance.NotifyPlayersPotChanged();
        }

        public bool isFirstBet(long SteamID)
        {
            bool firstBet = true;
            foreach (Bet bet in Bets)
            {
                if (bet.Player.SteamID == SteamID)
                    return false;
            }
            return firstBet;
        }


        private void InsertNewBet(long steamID, List<Dota2Item> items)
        {
            Bet NewBet = new Bet()
            {
                ItemsBet = items,
                Player = new Player()
                {
                    SteamID = steamID
                }
            };
            Bets.Add(NewBet);
            NetManagerInstance.NotifyPlayersPotChanged();
        }

        private DateTimeOffset ComputeBetEndTime()
        {
            OffSetTime = DateTimeOffset.Now.AddSeconds(90);
            return OffSetTime;
        }

        private DateTimeOffset IncreaseBetEndTime()
        {
            OffSetTime = OffSetTime.AddSeconds(15);
            return OffSetTime;
        }
    }

  
}
