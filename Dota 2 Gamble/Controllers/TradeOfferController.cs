﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dota_2_Gamble.Models;

namespace Dota_2_Gamble.Controllers
{
    public class TradeOfferController : ApiController
    {
        // GET: api/TradeOffer
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TradeOffer/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TradeOffer
        public void Post(Dota2Item itemRequested, SteamClient client)
        {
        }

        // PUT: api/TradeOffer/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TradeOffer/5
        public void Delete(int id)
        {
        }
    }
}
