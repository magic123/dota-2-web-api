﻿using Dota_2_Gamble.Models;
using Dota_2_Gamble.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static Dota_2_Gamble.Net.NetManager;
using static Dota_2_Gamble.Services.DBService;

namespace Dota_2_Gamble.Controllers
{
    [RoutePrefix("api/items")]
    public class ItemsController : ApiController
    {
       
        [HttpGet]
        public Dota2Item Get(string id)
        {
            return DBService.GetDBInstance().getDota2Item(id);
        }

        [HttpGet]
        public double GetPrice(long classid, long instanceid)
        {
            return DBService.GetDBInstance().getPrice(classid, instanceid);
        }

        [Route("GetPrice/{Market_Hash_Name}")]
        [HttpGet]
        public double GetPrice(string Market_Hash_Name)
        {
            return DBService.GetDBInstance().GetPrice(Market_Hash_Name);
        }

        // POST: api/Items
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Items/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Items/5
        public void Delete(int id)
        {
        }
       
        /// <summary>
        /// Sync the given player's inventory with the db, in order to get the image irul and more informations.
        /// </summary>
        /// <param name="SteamID"></param>
        /// <param name="items"></param>
        [HttpGet]
        [Route("SyncInventory/{SteamID:double}")]
        public String SyncPlayerInventory(long SteamID)
        {
            //First let's get the player's inventory as a JSON
            HttpResponseMessage responseMessage = GetHttpClient(Constants.PROFILE_BASE_URL).GetAsync(BuildPlayerInventoryURL(SteamID)).Result;
            string itemsAsJsonString = responseMessage.Content.ReadAsStringAsync().Result;

            JObject responseAsJSONObject = JObject.Parse(itemsAsJsonString);

            JProperty rgIventoryJProperty = responseAsJSONObject.Property("rgIventory");
            JProperty rgDescriptionsJProperty = responseAsJSONObject.Property("rgDescriptions");
            JToken properties = rgDescriptionsJProperty.Value;

            Dota2Item currentItem = null;
            JObject currentJSONObject = null;
            foreach(JProperty property in properties)
            {
                currentJSONObject = property.Value.Value<JObject>();
                currentItem = new Dota2Item();
                currentItem.MarketName = currentJSONObject["market_hash_name"].Value<String>();
                currentItem.MarketHashName = RemoveSpecialCharacters(currentItem.MarketName);
                currentItem.IconURL = currentJSONObject["icon_url"].Value<String>();
                GetDBInstance().insertOrUpdate(currentItem);
            }
            return "Inventory Synced !";
        }
    }
}
